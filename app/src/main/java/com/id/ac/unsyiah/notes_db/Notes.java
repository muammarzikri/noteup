package com.id.ac.unsyiah.notes_db;

public class Notes {

    String _id;
    String title_c;
    String group_c;
    String text_c;



    //Empty constructor
    public Notes(){

    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTitle_c() {
        return title_c;
    }

    public void setTitle_c(String title_c) {
        this.title_c = title_c;
    }

    public String getGroup_c() {
        return group_c;
    }

    public void setGroup_c(String group_c) {
        this.group_c = group_c;
    }

    public String getText_c() {
        return text_c;
    }

    public void setText_c(String text_c) {
        this.text_c = text_c;
    }

    //Constructor
    public Notes(String title_c, String group_c, String text_c){
        this.title_c=title_c;
        this.group_c=group_c;
        this.text_c=text_c;
    }

}