package com.id.ac.unsyiah.button_nav;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.id.ac.unsyiah.R;
import com.id.ac.unsyiah.notes_db.DataNotes;
import com.id.ac.unsyiah.notes_db.Notes;

import org.commonmark.node.Node;

import java.util.Objects;

import ru.noties.markwon.Markwon;

public class Read extends Fragment{
    String id;

    public void setId(String id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_read, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DataNotes db = new DataNotes(getContext());
        db.open();

        Notes notes = db.getNote(this.id);
        if(notes!=null){
            makeView(notes);
        }
        db.close();

    }

    private void makeView(Notes data){
        final Markwon markwon = Markwon.create(Objects.requireNonNull(getContext()));

        EditText title= Objects.requireNonNull(getView()).findViewById(R.id.input_title);
        EditText group=getView().findViewById(R.id.input_group);
        EditText teks=getView().findViewById(R.id.input_teks);
        title.setText(data.getText_c());
        group.setText(data.getGroup_c());
        teks.setText(data.getText_c());

        View container = getView().findViewById(R.id.readContainer);

        Node node = markwon.parse(title.getText().toString());

        Spanned markdown = markwon.render(node);
        markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_title), markdown);

        node = markwon.parse(group.getText().toString());
        markdown = markwon.render(node);
        markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_group), markdown);

        node = markwon.parse(teks.getText().toString());
        markdown = markwon.render(node);
        markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_teks), markdown);

    }
}
