package com.id.ac.unsyiah.notes_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataNotes {

    private SQLiteDatabase database;
    private DatabaseHelper dbhelper;

    public DataNotes(Context context) {
        dbhelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbhelper.getWritableDatabase();
    }

    public void close() {
        dbhelper.close();
    }

    public void addNotes(Notes notes) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.KEY_ID, notes.get_id());
        values.put(DatabaseHelper.KEY_TITLE, notes.getTitle_c());
        values.put(DatabaseHelper.KEY_GROUP, notes.getGroup_c());
        values.put(DatabaseHelper.KEY_TEXT, notes.getText_c());
        //inserting row
        Log.d("values", "addNotes: "+values.toString());

        database.insert(DatabaseHelper.TABLE_NOTES, null, values);
    }

    public List<Notes> getAllNotes() {
        List<Notes> listNotes = new ArrayList<Notes>();

        //select all data mahasiswa
        String allNotes = "SELECT * FROM " + DatabaseHelper.TABLE_NOTES;
        Cursor cursor = database.rawQuery(allNotes, null);

        //looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notes notes = new Notes();
                notes.set_id(Integer.parseInt(cursor.getString(0)) + "");
                notes.setTitle_c(cursor.getString(1));
                notes.setGroup_c(cursor.getString(2));
                notes.setText_c(cursor.getString(3));

                //adding mahasiswa to the list
                listNotes.add(notes);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listNotes;
    }


    public Notes getNote(String id) {
        String allNotes = "SELECT * FROM " + DatabaseHelper.TABLE_NOTES + " WHERE id_c=" + id;
        Cursor cursor = database.rawQuery(allNotes, null);
        Notes notes = new Notes();

        if (cursor.moveToFirst()) {
            notes.set_id(Integer.parseInt(cursor.getString(0)) + "");
            notes.setTitle_c(cursor.getString(1));
            notes.setGroup_c(cursor.getString(2));
            notes.setText_c(cursor.getString(3));
        }
        cursor.close();
        return notes;
    }

    public List<Notes> getNotes(String query) {
        List<Notes> listNotes = new ArrayList<Notes>();

        //select all data mahasiswa
        String allNotes = "SELECT * FROM " + DatabaseHelper.TABLE_NOTES + " WHERE title_c LIKE '%" + query + "%'";
        Cursor cursor = database.rawQuery(allNotes, null);
        //looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notes notes = new Notes();
                notes.set_id(Integer.parseInt(cursor.getString(0)) + "");
                notes.setTitle_c(cursor.getString(1));
                notes.setGroup_c(cursor.getString(2));
                notes.setText_c(cursor.getString(3));

                //adding mahasiswa to the list
                listNotes.add(notes);
                Log.d("db", "getNotes: " + notes.title_c);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listNotes;
    }

    public Notes getLastInsert() {
        String allNotes = "SELECT * FROM " + DatabaseHelper.TABLE_NOTES + " ORDER BY id_c DESC LIMIT 1";
        Cursor cursor = database.rawQuery(allNotes, null);
        Notes notes = new Notes();

        if (cursor.moveToFirst()) {
            notes.set_id(Integer.parseInt(cursor.getString(0)) + "");
            notes.setTitle_c(cursor.getString(1));
            notes.setGroup_c(cursor.getString(2));
            notes.setText_c(cursor.getString(3));
        }
        cursor.close();
        return notes;
    }
}