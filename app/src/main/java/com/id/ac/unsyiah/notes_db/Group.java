package com.id.ac.unsyiah.notes_db;

public class Group {

    String _id;
    String group_id;
    String note_id;

    public  Group(){}
    public Group(String id_group,String id_note){
        this.group_id =id_group;
        this.note_id =id_note;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getNote_id() {
        return note_id;
    }

    public void setNote_id(String note_id) {
        this.note_id = note_id;
    }
}