package com.id.ac.unsyiah.notes_db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "notes.db";

    public static final String TABLE_NOTES = "tb_notes";
    public static final String TABLE_GROUP = "tb_group";

    public static final String KEY_ID = "id_c";
    public static final String KEY_TITLE = "title_c";
    public static final String KEY_GROUP = "group_c";
    public static final String KEY_TEXT = "text_c";

    public static final String GKEY_ID = "id_c";
    public static final String GKEY_GROUP = "group_id";
    public static final String GKEY_NOTE = "note_id";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_GROUP= "CREATE TABLE " + TABLE_GROUP + "("
                + GKEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + GKEY_GROUP + " TEXT, " + GKEY_NOTE+ " TEXT)";
        db.execSQL(CREATE_TABLE_GROUP);

        String CREATE_TABLE_NOTES = "CREATE TABLE " + TABLE_NOTES + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_TITLE + " TEXT,"
                + KEY_GROUP + " TEXT," + KEY_TEXT +" TEXT)";
        db.execSQL(CREATE_TABLE_NOTES);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //drop tabel jika sudah pernah ada
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROUP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);

        //create table again
        onCreate(db);
    }

}