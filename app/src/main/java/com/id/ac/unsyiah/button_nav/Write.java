package com.id.ac.unsyiah.button_nav;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.id.ac.unsyiah.R;
import com.id.ac.unsyiah.notes_db.DataGroup;
import com.id.ac.unsyiah.notes_db.DataNotes;
import com.id.ac.unsyiah.notes_db.Group;
import com.id.ac.unsyiah.notes_db.Notes;

import org.commonmark.node.Node;

import java.util.Objects;

import ru.noties.markwon.Markwon;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Write extends Fragment{
    boolean flagControl;
    boolean readMode;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        flagControl=false;
        readMode=false;
        return inflater.inflate(R.layout.fragment_write, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button menuEditor = Objects.requireNonNull(getView()).findViewById(R.id.editorControl);
        menuEditor.setOnClickListener(new ButtonControl());

        Button readMode = getView().findViewById(R.id.viewOnOff);
        readMode.setOnClickListener(new ButtonReadMode());

        Button saveNote = getView().findViewById(R.id.save);
        saveNote.setOnClickListener(new ButtonSave());
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

    }


    private class ButtonReadMode implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(!readMode){
                final Markwon markwon = Markwon.create(Objects.requireNonNull(getContext()));

                EditText title= Objects.requireNonNull(getView()).findViewById(R.id.input_title);
                EditText group=getView().findViewById(R.id.input_group);
                EditText teks=getView().findViewById(R.id.input_teks);

                title.setVisibility(View.INVISIBLE);
                group.setVisibility(View.INVISIBLE);
                teks.setVisibility(View.INVISIBLE);

                getView().findViewById(R.id.editorControl).setVisibility(View.INVISIBLE);

                View container = getView().findViewById(R.id.readContainer);


                Node node = markwon.parse(title.getText().toString());

                Spanned markdown = markwon.render(node);
                markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_title), markdown);

                node = markwon.parse(group.getText().toString());
                markdown = markwon.render(node);
                markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_group), markdown);

                node = markwon.parse(teks.getText().toString());
                markdown = markwon.render(node);
                markwon.setParsedMarkdown((TextView) container.findViewById(R.id.read_input_teks), markdown);

                closeEditor();
                container.setVisibility(View.VISIBLE);

                readMode=true;
            }else {
                Objects.requireNonNull(getView()).findViewById(R.id.input_title).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.input_group).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.input_teks).setVisibility(View.VISIBLE);
                getView().findViewById(R.id.readContainer).setVisibility(View.INVISIBLE);


                getView().findViewById(R.id.editorControl).setVisibility(View.VISIBLE);

                readMode=false;
            }

        }
    }
    public void hideKeyboard(){
        Context c = getActivity().getBaseContext();
        View v = getView().findFocus();
        if(v == null)
            return;
        InputMethodManager inputManager = (InputMethodManager) c.getSystemService(INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void closeEditor(){
        flagControl=true;
        Objects.requireNonNull(getView()).findViewById(R.id.editorControl).performClick();
    }

    private class ButtonControl implements View.OnClickListener{
        @Override
        public void onClick(View v) {

            hideKeyboard();

            int flag ;
            if (!flagControl) {
                flag = View.VISIBLE;
                flagControl=true;
            }else {
                flag = View.INVISIBLE;
                flagControl=false;
            }
            getView().findViewById(R.id.menu1).setVisibility(flag);
            getView().findViewById(R.id.menu2).setVisibility(flag);
            getView().findViewById(R.id.menu3).setVisibility(flag);
            getView().findViewById(R.id.menu4).setVisibility(flag);

        }
    }

    private class ButtonReadNote implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            String title = ((EditText)getView().findViewById(R.id.input_title)).getText().toString();
            String group = ((EditText)getView().findViewById(R.id.input_group)).getText().toString();
            String notes = ((EditText)getView().findViewById(R.id.input_teks)).getText().toString();

            Log.d("view ", "onClick: "+title+group+notes);
        }
    }


    private class InputTextMD implements View.OnKeyListener {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            final TextView textView = v.findViewById(R.id.input_teks);
            final Markwon markwon = Markwon.create(getActivity());

            final Node node = markwon.parse(textView.getText().toString());
            final Spanned markdown = markwon.render(node);

            if (keyCode==KeyEvent.KEYCODE_SPACE || keyCode == KeyEvent.KEYCODE_ENTER) {
                //do something here
                Log.d("press", "onKey: Toast space enter");
                Log.d("press", "onKey: md " + markdown);
                markwon.setParsedMarkdown(textView, markdown);
                return true;
            }else {
                Log.d("press", "onKey: not Toast space enter");
                Log.d("press", "onKey: md "+markdown);
//                markwon.setParsedMarkdown(textView, markdown);
            }
            return false;
        }
    }

    private class ButtonSave implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            DataNotes db = new DataNotes(getContext());
            db.open();
            //insert data mahasiswa
            Log.d("aa: ", "Masukkan data notes......" +((EditText)getView().findViewById(R.id.input_title)).getText().toString());

            Notes data= new Notes(
                    ((EditText)getView().findViewById(R.id.input_title)).getText().toString(),
                    ((EditText)getView().findViewById(R.id.input_group)).getText().toString(),
                    ((EditText)getView().findViewById(R.id.input_teks)).getText().toString()
            );
            db.addNotes(data);

            DataGroup dbG = new DataGroup(getContext());
            @SuppressLint("CutPasteId") Group group= new Group(
                    ((EditText)getView().findViewById(R.id.input_group)).getText().toString(),
                    db.getLastInsert().get_id()
                );
//            dbG.addGroup(group);
            dbG.close();
            db.close();

        }
    }
}
