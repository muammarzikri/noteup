package com.id.ac.unsyiah.button_nav;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.text.Spanned;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.id.ac.unsyiah.R;
import com.id.ac.unsyiah.notes_db.DataNotes;
import com.id.ac.unsyiah.notes_db.Notes;

import org.commonmark.node.Node;

import java.util.Objects;

import ru.noties.markwon.Markwon;

public class Home extends Fragment{
    int margin_1=0;
    int margin_2=300;
    String del_id;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView search = getView().findViewById(R.id.input_search);
        search.setOnKeyListener(new InputTextMD());

        DataNotes db = new DataNotes(getContext());
        final ScrollView scrollView = Objects.requireNonNull(getView()).findViewById(R.id.scrollViewC);
        final LinearLayout linearLayout = new LinearLayout(getContext());
        db.open();

        LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(layoutparams);

        Log.d("Reading: ", "Baca data mahasiswa");
        java.util.List<Notes> listNotes = db.getAllNotes();

        for (Notes notes : listNotes) {
            String log = "ID " +notes.get_id() + " ,getTitle_c: " + notes.getTitle_c() + " ,getGroup_c: " + notes.getGroup_c() + " ,getText_c : " +notes.getText_c();
            Log.d("Semua Data : ", log);
            linearLayout.addView(createCardView(notes));

        }
        scrollView.addView(linearLayout);
        db.close();
    }

    @SuppressLint("ClickableViewAccessibility")
    public CardView createCardView(final Notes data){
        CardView cardview;

        LinearLayout.LayoutParams layoutparams;
        cardview = new CardView(getContext());

        layoutparams = new LinearLayout.LayoutParams(
                500,
                350
        );
        layoutparams.topMargin=60;

        cardview.setLayoutParams(layoutparams);
        cardview.setRadius(5);
        cardview.setPadding(25, 25, 25, 25);
        cardview.setMaxCardElevation(30);

        TextView textView2= new TextView(getContext());

        textView2.setVisibility(View.INVISIBLE);
        textView2.setText(data.get_id());

        TextView textview = new TextView(getContext());
        layoutparams=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                200
        );
        textview.setLayoutParams(layoutparams);
        textview.setText(data.get_id()+"\n"+data.getTitle_c());
//        textview.setBackgroundColor(Color.RED);
        textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        textview.setTextColor(Color.BLACK);

        textview.setPadding(230,25,25,25);
//        textview.setGravity(Gravity.LEFT);

        cardview.addView(textview);
        cardview.addView(textView2);

        cardview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Read read = new Read();
                read.setId(data.get_id());
                loadFragment(read);
            }
        });

        cardview.setOnLongClickListener(new DialogCard2());
        return cardview;

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            Log.d("click", "onClick: card");

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    private class InputTextMD implements View.OnKeyListener {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            final TextView textView = v.findViewById(R.id.input_search);
            if ( keyCode == KeyEvent.KEYCODE_ENTER) {
                Log.d("button", "onKey: KEYCODE_ENTER");
                //do something here
                DataNotes db = new DataNotes(getContext());
                final ScrollView scrollView = Objects.requireNonNull(getView()).findViewById(R.id.scrollViewC);
                scrollView.removeAllViewsInLayout();

                final LinearLayout linearLayout = new LinearLayout(getContext());
                db.open();

                LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                );
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setLayoutParams(layoutparams);

                Log.d("Reading: ", "Baca data mahasiswa");
                java.util.List<Notes> listNotes = db.getNotes(textView.getText().toString());

                for (Notes notes : listNotes) {
                    String log = "ID " +notes.get_id() + " ,getTitle_c: " + notes.getTitle_c() + " ,getGroup_c: " + notes.getGroup_c() + " ,getText_c : " +notes.getText_c();
                    Log.d("Semua Data : ", log);
                    linearLayout.addView(createCardView(notes));

                }
                scrollView.addView(linearLayout);
                db.close();
                return true;
            }
            return false;
        }
    }



    private class DialogCard2 implements View.OnLongClickListener{

        @Override
        public boolean onLongClick(View v) {
            ViewGroup vg = (ViewGroup)v;
            final Dialog fbDialogue = new Dialog(getActivity());
            fbDialogue.setContentView(R.layout.custom_layout);
            fbDialogue.setCancelable(true);
            fbDialogue.show();
            Button del=fbDialogue.findViewById(R.id.delete);
            del_id=((TextView)vg.getChildAt(2)).getText().toString();
            del.setOnClickListener(new DelButton());
//            Log.d("del", "onTouch: "+();
            return false;
        }
    }

    private class DelButton implements View.OnClickListener {
        @Override
        public void onClick(View v) {
        }
    }
    private class DialogCard implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final Dialog fbDialogue = new Dialog(getActivity());
            fbDialogue.setContentView(R.layout.custom_layout);
            fbDialogue.setCancelable(true);
            fbDialogue.show();
            return true;
        }
    }
}

