package com.id.ac.unsyiah.button_nav;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Spanned;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.id.ac.unsyiah.R;
import com.id.ac.unsyiah.notes_db.DataGroup;
import com.id.ac.unsyiah.notes_db.DataNotes;
import com.id.ac.unsyiah.notes_db.Notes;

import org.commonmark.node.Node;

import java.util.Objects;

import ru.noties.markwon.Markwon;

public class Group extends Fragment{

    Dialog myDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_group, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        Button addGroup = Objects.requireNonNull(getView()).findViewById(R.id.addGroup);

        addGroup.setOnClickListener(new AddGroup());

        DataGroup db = new DataGroup(getContext());
        final ScrollView scrollView = Objects.requireNonNull(getView()).findViewById(R.id.scrollViewC);
        final LinearLayout linearLayout = new LinearLayout(getContext());
        db.open();

        LinearLayout.LayoutParams layoutparams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(layoutparams);

        java.util.List<com.id.ac.unsyiah.notes_db.Group> listNotes = db.getAllGroup();

        for (com.id.ac.unsyiah.notes_db.Group group : listNotes) {
            linearLayout.addView(createCardView(group));
        }
        scrollView.addView(linearLayout);
        db.close();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myDialog = new Dialog(Objects.requireNonNull(getActivity()));

    }

    public CardView createCardView(final com.id.ac.unsyiah.notes_db.Group data){
        CardView cardview;

        LinearLayout.LayoutParams layoutparams;
        cardview = new CardView(getContext());

        layoutparams = new LinearLayout.LayoutParams(
                500,
                350
        );
        layoutparams.topMargin=60;

        cardview.setLayoutParams(layoutparams);
        cardview.setRadius(5);
        cardview.setPadding(25, 25, 25, 25);
        cardview.setMaxCardElevation(30);

        TextView textView2= new TextView(getContext());

        textView2.setVisibility(View.INVISIBLE);
        textView2.setText(data.get_id());

        TextView textview = new TextView(getContext());
        layoutparams=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                200
        );
        textview.setLayoutParams(layoutparams);
//        textview.setBackgroundColor(Color.RED);
        textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        textview.setTextColor(Color.BLACK);

        textview.setPadding(230,25,25,25);
//        textview.setGravity(Gravity.LEFT);

        cardview.addView(textview);
        cardview.addView(textView2);

        cardview.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                Read read = new Read();
//                read.setId(data.getId());
//                loadFragment(read);
            }
        });
        return cardview;

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            Log.d("click", "onClick: card");

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    private class InputTextMD implements View.OnKeyListener {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            final TextView textView = v.findViewById(R.id.input_teks);
            final Markwon markwon = Markwon.create(getActivity());

            final Node node = markwon.parse(textView.getText().toString());
            final Spanned markdown = markwon.render(node);

            if ( keyCode == KeyEvent.KEYCODE_ENTER) {
                //do something here
                Log.d("press", "onKey: Toast space enter");
                Log.d("press", "onKey: md " + markdown);
                markwon.setParsedMarkdown(textView, markdown);
                return true;
            }
            return false;
        }
    }

    private class AddGroup implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            final Dialog fbDialogue = new Dialog(getActivity());
            fbDialogue.setContentView(R.layout.custom_layout);
            fbDialogue.setCancelable(true);
            fbDialogue.show();
        }
    }
}
