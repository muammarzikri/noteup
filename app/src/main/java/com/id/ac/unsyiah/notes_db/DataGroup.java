package com.id.ac.unsyiah.notes_db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataGroup {

    private SQLiteDatabase database;
    private DatabaseHelper dbhelper;

    public DataGroup(Context context){
        dbhelper = new DatabaseHelper(context);
    }
    public void open() throws SQLException {
        database = dbhelper.getWritableDatabase();
    }

    public void close(){
        dbhelper.close();
    }

    public void addGroup(Group group){
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.GKEY_ID, group.get_id());
        values.put(DatabaseHelper.GKEY_GROUP, group.getGroup_id());
        values.put(DatabaseHelper.GKEY_NOTE, group.getNote_id());

        //inserting row

        database.insert(DatabaseHelper.TABLE_GROUP, null, values);
    }

    public List<Group> getAllGroup(){
        List<Group> listGroup = new ArrayList<Group>();

        //select all data mahasiswa
        try {
            String allGroup = "SELECT * FROM " + DatabaseHelper.TABLE_GROUP;
            Cursor cursor = database.rawQuery(allGroup, null);

            //looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Group group = new Group();
                    group.set_id(Integer.parseInt(cursor.getString(0)) + "");
                    group.setGroup_id(cursor.getString(1));
                    group.setNote_id(cursor.getString(2));

                    //adding mahasiswa to the list
                    listGroup.add(group);
                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            Log.d("e", "getAllGroup: "+e);
        }
        return listGroup;
    }


    public Group getGroup(String id){
        String allGroup = "SELECT * FROM " + DatabaseHelper.TABLE_GROUP+" WHERE _id="+id;
        Cursor cursor = database.rawQuery(allGroup, null);
        Group group = new Group();

        if (cursor.moveToFirst()) {
            group.set_id(Integer.parseInt(cursor.getString(0)) + "");
            group.setGroup_id(cursor.getString(1));
            group.setNote_id(cursor.getString(2));
        }
        cursor.close();
        return group;
    }

    public Group getLastInsert(){
        String allGroup = "SELECT * FROM " + DatabaseHelper.TABLE_GROUP+" ORDER BY id_c DESC LIMIT 1";
        Cursor cursor = database.rawQuery(allGroup, null);
        Group group = new Group();

        if (cursor.moveToFirst()) {
            group.set_id(Integer.parseInt(cursor.getString(0)) + "");
            group.setGroup_id(cursor.getString(1));
            group.setNote_id(cursor.getString(2));
        }
        cursor.close();
        return group;
    }
}